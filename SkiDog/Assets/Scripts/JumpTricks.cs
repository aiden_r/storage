﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpTricks : MonoBehaviour
{
    
   void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(Rotate(Vector3.forward, 180, .5f));
            
        }
    }

    IEnumerator Rotate(Vector3 axis, float angle, float duration)
    {
        Quaternion from = transform.rotation;
        Quaternion to = transform.rotation;
        to *= Quaternion.Euler(axis * angle);

        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            transform.rotation = Quaternion.Slerp(from, to, elapsed / duration);
            elapsed += Time.deltaTime;
            yield return null;
        }
        
        Quaternion from1 = transform.rotation;
        Quaternion to1 = transform.rotation;
        to *= Quaternion.Euler(axis * angle);

        float elapsed1 = 0.0f;
        while (elapsed1 < duration)
        {
            transform.rotation = Quaternion.Slerp(from, to, elapsed / duration);
            elapsed += Time.deltaTime;
            yield return null;
        }
       
        StopAllCoroutines();
    }
}

