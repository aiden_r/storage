﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HatSpawner : MonoBehaviour
{

    public float rotationX;
    public GameObject[] obj;
    private GameObject randomObject;

    Vector3 spawn;
    bool nextJump = true;
    public static Vector3 spawner;
    // Start is called before the first frame update
    void Start()
    {
        Spawn();
    }

    // Update is called once per frame
    void Update()
    {
        

    }
    //add distance between Jumps
 
    void Spawn()
    { // Intatiate a jump at the spawn location
        spawn = new Vector3(transform.position.x, transform.position.y, transform.position.z + Random.Range(-40, 40));
        spawner = spawn;
        randomObject = obj[Random.Range(0, obj.Length)];

    
            rotationX = randomObject.transform.rotation.x;
            nextJump = false;
            Instantiate(randomObject, spawn, randomObject.transform.rotation = Quaternion.Euler(rotationX, Random.Range(0, 180), 0));
            
        

    }
}
