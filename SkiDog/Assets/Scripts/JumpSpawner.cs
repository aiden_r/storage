﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpSpawner : MonoBehaviour
{
    public float distance = 5f;
    public GameObject[] jump;
    Vector3 spawn;
    bool nextJump = true;
    // Start is called before the first frame update
    void Start()
    {
        spawn = transform.position;
        Trick.score -= 1;
    }

    // Update is called once per frame
    void Update()
    {
        Jumps();
     
    }

    void Jumps()
    { // Intatiate a jump at the spawn location
        if (nextJump == true && Trick.score < 3)
        {
            nextJump = false;
            Instantiate(jump[0], spawn, Quaternion.identity);
            
        }
        if (nextJump == true && Trick.score > 3)
        {
            nextJump = false;
            Instantiate(jump[1], spawn, Quaternion.identity);

        }

    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "piste")
        {
            nextJump = true;
            Debug.Log("spawn");
        }
    }
}
