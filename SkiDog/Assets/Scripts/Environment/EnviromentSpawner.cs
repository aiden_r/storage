﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnviromentSpawner : MonoBehaviour
{
    public float rotationX;
    public GameObject[] obj;
    private GameObject randomObject;

    Vector3 spawn;
    bool nextJump = true;
    public static Vector3 spawner;
    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        Spawn();
      
    }
    //add distance between Jumps
    IEnumerator DistanceBetweenJumps()
    {
        yield return new WaitForSeconds(Random.Range(.5f,4));
        nextJump = true;
    }
    void Spawn ()
    { // Intatiate a jump at the spawn location
        spawn = new Vector3(transform.position.x, transform.position.y, transform.position.z + Random.Range(-30, 30));
        spawner = spawn;
        randomObject = obj[Random.Range(0, obj.Length)];
        
        if (nextJump == true)
        {
            rotationX = randomObject.transform.rotation.x;
            nextJump = false;
            Instantiate(randomObject, spawn, randomObject.transform.rotation = Quaternion.Euler(rotationX,Random.Range(0,180),0));
            StartCoroutine(DistanceBetweenJumps());
        }

    }
}
