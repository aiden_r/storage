﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnviromentSizeRandomizer : MonoBehaviour
{
    public Vector3 offset;

    
    // Start is called before the first frame update
    void Start()
    {
        
        
        float size = Random.Range(5, 20);
        this.transform.localScale = new Vector3(size, size, size);
       gameObject.transform.position +=  offset;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 trajectory = new Vector3(-19.696f, 3.473f, 0f);
        transform.Translate(trajectory * Time.deltaTime * PupperController.speed, Space.World);
    }
}
