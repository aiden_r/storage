﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiggerRocks : MonoBehaviour
{

    float size;
    // Start is called before the first frame update
    void Start()
    {
        size = 500;
        this.transform.localScale = new Vector3(size, size, size);
    }

}
