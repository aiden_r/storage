﻿using UnityEngine;

public class HatStack : MonoBehaviour
{
    private Vector3 nextHat;
    public float newPos;
    
    public GameObject spawner;
    Collider col;
    // Start is called before the first frame update
    void Update()
    {
        nextHat = spawner.transform.position;
        if (PupperController.hitRock)
        {
            newPos = 0;
        }
    }

   void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Hat" && !PupperController.hitRock )
        {
            
            Destroy(other.gameObject);
            nextHat.y += newPos;
            GameObject newHat = Instantiate(other.gameObject, nextHat, Quaternion.identity);
            
            newHat.GetComponent<JumpLevelOne>().enabled = false;         
            newHat.transform.parent = spawner.transform;
            newHat.GetComponent<Hat>().enabled = true;



            newPos += 1.5f;
            
        }

    }
}
