﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hat : MonoBehaviour
{

    // Start is called before the first frame update
  

    // Update is called once per frame
    void Update()
    {
        if (PupperController.hitRock)
        {

            transform.parent = null;
            this.gameObject.AddComponent<Rigidbody>();
            StartCoroutine(HatDestroy());
        }
    }
    IEnumerator HatDestroy()
    {
        yield return new WaitForSeconds(5);
        Destroy(this.gameObject);
        PupperController.hitRock = false;

    }
}
