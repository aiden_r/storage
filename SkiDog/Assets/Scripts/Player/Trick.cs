﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Trick : MonoBehaviour
{
    private string[] tricks = new string[] { "Fire1", "Fire2", "Fire3", "Jump" };
    private string trickButton;
    public static bool hitTrick;
    public ParticleSystem firework;
    private bool zone= false;
    private Image spriteRenderer;
    private string[] psButtons = new string[] { " Square ", " Circle ", " Triangle ", "X" };
    private int rando;
    public Sprite[] sprites;
    public static int score;
    GameObject psbutton;
    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GameObject.FindGameObjectWithTag("Button").GetComponent<Image>();
      
        rando = Random.Range(0, 4);
        trickButton = tricks[rando];
        firework = GetComponent<ParticleSystem>();
        Debug.Log(trickButton);
       


    }
    void Update()
    {
        if (Input.GetAxis(trickButton) > 0 && zone == true)
        {
            hitTrick = true;
            
            
            
        }
      
    }
    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
        Debug.Log("/IN the ZONE");
            zone = true;
            
            spriteRenderer.sprite = sprites[rando];
            spriteRenderer.enabled = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            zone = false;
            spriteRenderer.sprite = null;
        }
        if (hitTrick)
        {
            score += 1;
            
        }
   
        hitTrick = false;
    }
    

}
