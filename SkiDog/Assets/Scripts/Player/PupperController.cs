﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PupperController : MonoBehaviour

{
    
    private Vector3 movement;
    private Quaternion startRotation;
    private Vector3 faceForward;
    public Animator anim;
    public float rotSpeed = 5;
    private float speedFall = 2;
    public static float speed = 0.5f;
    public float thrust;
    public float GravityMultipler;
    float AddGravity;
    public static bool hitRock = false;
    private bool isAxisInUse = false;
    Trick trick;
    Rigidbody rb;
    LayerMask layerMask;
    
    private bool isGround;
    // Start is called before the first frame update
    void Start()
    {
        

        rb = GetComponent<Rigidbody>();

       
        trick = new Trick();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        int layerMask = 1 << 8;
        AddGravity = GravityMultipler;
        rb.AddForce((Vector3.down * AddGravity), ForceMode.Acceleration);

        Controls();

        RaycastHit hit;
        if (Physics.Raycast(transform.position,Vector3.down,out hit, speedFall, layerMask))
        {
           
            isGround = true;
        }
        else
        {
            isGround = false;
        }

    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("ground");

   
        if (other.tag == "Rock")
        {
            hitRock = true;
        }
        
    }
   
    void Controls()
    {
        transform.LookAt(transform.position + new Vector3(-Input.GetAxis("Horizontal"), 0, .5f));
     
            speed += Input.GetAxis("Vertical") / 50 - Mathf.Abs(Input.GetAxis("Horizontal")/100) ;
            if(speed <= 0.5f)
            {
                speed = 0.5f;
            }
            if (speed > 3.5f)
            {
                speed = 3.5f;
            }
          
            
           
            movement = new Vector3(rb.velocity.x, rb.velocity.y, Input.GetAxisRaw("Horizontal") *20);
        if (isGround)
        {

            // X button
            if (Input.GetAxisRaw("Jump") != 0)
            {
                if (isAxisInUse == false)
                {
                    rb.AddForce(Vector3.up*thrust, ForceMode.VelocityChange);
                    anim.Play("Backflip");
                    isAxisInUse = true;
                }
            }
            if (Input.GetAxisRaw("Jump") == 0)
            {
                isAxisInUse = false;
            }
            //Square
            if (Input.GetAxisRaw("Fire1") != 0)
            {
                if (isAxisInUse == false)
                {
                    rb.AddForce(Vector3.up * thrust, ForceMode.VelocityChange);
                    anim.Play("Roll");
                    isAxisInUse = true;
                }
            }
            if (Input.GetAxisRaw("Fire1") == 0)
            {
                isAxisInUse = false;
            }

            // Circle
            if (Input.GetAxisRaw("Fire2") != 0)
            {
                if (isAxisInUse == false)
                {
                    rb.AddForce(Vector3.up * thrust, ForceMode.Impulse);
                    anim.Play("method");

                    isAxisInUse = true;
                }
            }
            if (Input.GetAxisRaw("Fire2") == 0)
            {
                isAxisInUse = false;
            }
            //Triangle
            if (Input.GetAxisRaw("Fire3") != 0)
            {
                if (isAxisInUse == false)
                {
                    rb.AddForce(Vector3.up * thrust, ForceMode.Impulse);
                    anim.Play("revMethod");
                    isAxisInUse = true;
                }
            }
            if (Input.GetAxisRaw("Fire3") == 0)
            {
                isAxisInUse = false;
            }




            rb.velocity = movement;
        }

        
    }
 
}
