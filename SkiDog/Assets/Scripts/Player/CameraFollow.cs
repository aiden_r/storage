﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform player;
    public float smooth = 10f;
    public Vector3 cameraoffset;
    private Vector3 velocity = Vector3.zero;
    void LateUpdate()
    {
        Vector3 desiredPosition = player.position + cameraoffset;
        Vector3 smoothedPostion = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, smooth * Time.deltaTime);
        transform.position = smoothedPostion;
            
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
