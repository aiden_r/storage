﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpLevelOne : MonoBehaviour
{
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        Vector3 trajectory = new Vector3(-19.696f, 3.473f, 0f);
        transform.Translate(trajectory * Time.deltaTime * PupperController.speed, Space.World);
    }
}
